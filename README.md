# README #
### Quick script to validate SIP Auth creds in a Registration to Broadworks ###

* just run the script with a python 3.6 or greater interpreter (python 3.6 or greater installed on your computer). 

### How does it work? ###
* $ python3 sipAuth.py

### How do I get set up? ###

* Just a script to gather user inputs via command line, no validations are done on the inputs.  There is no GUI
* Python 3.6+ is required



### Who do I talk to? ###

* JJ Wager @ Averistar
