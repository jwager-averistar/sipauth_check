# python 3.6
from hashlib import md5

# header from 401 response on REGISTER
# Sample data: Authorization: Digest username="5977499803",realm="voip.com",nonce="BroadWorksXkv9nk3k0Tu7vy3lBW",uri="sip:voip.com",response="b4717ceb3f1b6ce6c4a87ffa32dddf0d",algorithm=MD5,cnonce="5ffa6359",qop=auth,nc=00000021

def hash_response():
    
    print('Sample data: Authorization: Digest username="5977499803",realm="voip.com",nonce="BroadWorksXkv9nk3k0Tu7vy3lBW",uri="sip:voip.com",response="b4717ceb3f1b6ce6c4a87ffa32dddf0d",algorithm=MD5,cnonce="5ffa6359",qop=auth,nc=00000021\n')
    username = input ("Input the Auth username: ")
    uri = input ("Input the uri: ")
    nonce = input ("Input the nonce: ")
    realm = input ("Input the realm: ")
    password = input ("Input the confirmed SIP password from Broadworks: ")
    cnonce= input ("Input the cnonce: ")
    nc = input ("Input the nc value: ")
    qop = str('auth')

    str1 = md5("{}:{}:{}".format(username,realm,password).encode('utf-8')).hexdigest()
    str2 = md5("REGISTER:{}".format(uri).encode('utf-8')).hexdigest()
    str3 = md5("{}:{}:{}:{}:{}:{}".format(str1,nonce,nc,cnonce,qop,str2).encode('utf-8')).hexdigest()
    print("This value should be in the response= field of the Authorization header...  ", str3)

hash_response()